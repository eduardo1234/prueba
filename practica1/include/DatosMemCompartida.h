
#include <stdio.h>//entrada y salida de texto
#include <sys/stat.h>//uso de la libreria fifo
#include <sys/types.h>//uso de la libreria FIFO
#include <errno.h>//mensaje de error
#include <stdlib.h> //gestión de memoria dinámica y de procesos
#include <string.h> //cadena de caracteres
#include <sys/stat.h>// para fifo
#include <fcntl.h>
#include <unistd.h>

#pragma once 
#include "Esfera.h"
#include "Raqueta.h"
class DatosMemCompartida
{       
public:         
      Esfera esfera;
      Raqueta raqueta1;
      int accion; //1 arriba, 0 nada, -1 abajo
};

