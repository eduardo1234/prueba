// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <stdio.h>//entrada y salida de texto
#include <sys/stat.h>//uso de la libreria fifo
#include <sys/types.h>//uso de la libreria FIFO
#include <errno.h>//mensaje de error
//#include <stdlib.h> //gestión de memoria dinámica y de procesos
//#include <string.h> //cadena de caracteres
#include <sys/stat.h>// para fifo
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{		//sprintf(buffer,"Fin del programa");
		//write(fd,buffer,sizeof(buffer));
		//close(fd);
//cerramos la tuberia c-s de comunicación
close(fdcs);
unlink("/tmp/mififo_cs");
//cerramos la tubería de teclas
close(fdt);
unlink("/tmp/mififo_t");

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	char buff[1024];





// leemos los datos de la tuberia cliente-servidor
	char cad[1024];
	read(fdcs,buff,sizeof(buff));

//actualizar los atributos de mundocliente con los datos recibidos de la tuberia

  sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); 






//igualamos raqueta de memoria compartida con jugador1 y a la esfera para que la siga
	pdatos->raqueta1=jugador1;
	pdatos->esfera=esfera;
//imprime valor de acción en la memoria compartida
	printf("&d\n",Datos.accion);
//comparar acción para mover
	if(pdatos->accion==1)OnKeyboardDown('w',0,0);
	else if(pdatos->accion==-1)OnKeyboardDown('s',0,0);
	else if(pdatos->accion==0)OnKeyboardDown('r',0,0);

	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	//	sprintf(buffer,"jugador 2 marca 1 punto, lleva un total de %d puntos",puntos2);//apartado 5
	//	write(fd,buffer,sizeof(buffer));
		
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		//sprintf(buffer,"jugador 1 marca 1 punto, lleva un total de %d puntos",puntos1);//apartado 5
		//write(fd,buffer,sizeof(buffer));
	}

	if(puntos1==3)
	{	printf("el jugador1 ha ganado con %d puntos\n",puntos1);
		exit(0);
	}
	if(puntos2==3)
	{	printf("el jugador2 ha ganado con %d puntos\n",puntos2);
		exit(0);
	}
	
	if(esfera.radio> 0.2)
	{
		if(puntos1+puntos2!=0)
		{
			esfera.radio-=0.005f;
		}
	}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
		//escribir datos en la tuberia
	char cad[1024];
	sprintf(cad,"%c",key);
	write(fdt,cad,sizeof(cad));

	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	case 'r':jugador1.velocidad.y=0;break;

	}
}

void CMundo::Init()
{
//	fd=open("/tmp/mi_fifo",O_WRONLY);// apartado 4

// creando fichero del tamaño de MemCompartida

	int fdbot=open("/tmp/bot",O_CREAT|O_RDWR,0777);
	write(fdbot,&Datos,sizeof(Datos));	//para que fdbot acceda a los datos de la memoria compartida y lees los datos y la cantidad

//proyectamos el bot en la memoria compartida
	pdatos=(DatosMemCompartida*) mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fdbot,0);
//cerramos fdbot

close(fdbot);
printf("%ld\n",pdatos);
	
	//creamos tuberia cliente-servidor y la abrimos
	
	 mkfifo("/tmp/mififo_cs",0777);
	fdcs=open("/tmp/mififo_cs",O_RDONLY);

// creamos tubería en modod escritura

mkfifo("/tmp/mififo_t",0777);
fdt=open("/tmp/mififo_t",O_WRONLY);





	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
