// bot con proyección de memoria en fichero

#include <stdio.h>//entrada y salida de texto
#include <sys/stat.h>//uso de la libreria fifo
#include <sys/types.h>//uso de la libreria FIFO
#include <errno.h>//mensaje de error
#include <stdlib.h> //gestión de memoria dinámica y de procesos
#include <string.h> //cadena de caracteres
#include <sys/stat.h>// para fifo
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include "DatosMemCompartida.h"

using namespace std;

int main(){
//declarar puntero a DAtosMEmCompartida
DatosMemCompartida* pdatos;

//abrir el fichero proyectado en memoria creado en tenis
int fdbot=open("/tmp/bot",O_RDWR);


//protectar en una nueva memoria (DMC2) de bot y actualiza su contenido con lo que hay en DAtosMemCompartida de Mundo.cpp(DMC1)
if(fdbot<0)  perror("Error al abrir el bot 1\n");
pdatos=(DatosMemCompartida*) mmap(NULL, sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fdbot,0);
if (pdatos<0) perror("Error al generar la memoria compartida 1\n");
close(fdbot);
printf("%ld\n", pdatos);

while(1)
{
	if((pdatos->raqueta1.y1)<(pdatos->esfera.centro.y))pdatos->accion=1;
	else if((pdatos->raqueta1.y2)>(pdatos->esfera.centro.y))pdatos->accion=-1;

	usleep(25000);}
// cerrando el fichero
return 0;
}

